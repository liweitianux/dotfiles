#
# zsh/param.zsh
# see zshparam(1)
#

# Ask only if the top of the listing would scroll off the screen
LISTMAX=0

# Report consuming time statistics if user+system greater than 60 seconds
REPORTTIME=60

# Format of process time reports with the `time' keyword
TIMEFMT="%J  %U user %S system %P cpu %MM memory %*E total"
