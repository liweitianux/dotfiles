##
## ~/.notmuch-config
## Configuration file for the notmuch mail system
## http://notmuchmail.org
##
## IMPORTANT NOTE FOR CJK:
## | To support correct CJK characters index and search (i.e., proper
## ! character segmentation), the environment variable 'XAPIAN_CJK_NGRAM'
## | should be set to non-empty value before notmuch indexing.
##
## Aaron LI <aaronly@gmail.com>
## Created: 2015-02-02
## Updated: 2016-01-26
##

# Database configuration
#
# The only value supported here is 'path' which should be the top-level
# directory where your mail currently exists and to where mail will be
# delivered in the future. Files should be individual email messages.
# Notmuch will store its database within a sub-directory of the path
# configured here named ".notmuch".
#
[database]
path=/home/aly/mail

# User configuration
#
# Here is where you can let notmuch know how you would like to be
# addressed. Valid settings are
#
#	name		Your full name.
#	primary_email	Your primary email address.
#	other_email	A list (separated by ';') of other email addresses
#			at which you receive email.
#
# Notmuch will use the various email addresses configured here when
# formatting replies. It will avoid including your own addresses in the
# recipient list of replies, and will set the From address based on the
# address to which the original email was addressed.
#
[user]
name=Aaron LI
primary_email=aaronly.me@gmail.com
other_email=aaronly.me@outlook.com;liweitianux@autistici.org;liweitianux@gmail.com;liweitianux@live.com;liweitianux@sjtu.edu.cn;


# Configuration for "notmuch new"
[new]
#
# A list (separated by ';') of the tags that will be added to all messages
# incorporated by "notmuch new".
#
# Required by 'afew'
#tags=unread;inbox;
tags=new
#
# A list (separated by ';') of file and directory names that will not be
# searched for messages by "notmuch new".
#
# NOTE: *Every* file/directory that goes by one of those names will be
# ignored, independent of its depth/location in the mail store.
ignore=backup;offlineimap;


# Search configuration
[search]
#
# A ';'-separated list of tags that will be excluded from search results by
# default.  Using an excluded tag in a query will override that exclusion.
exclude_tags=deleted;spam;


# Maildir compatibility configuration
[maildir]
#
# Valid values are true and false.
#
# If true, then the following maildir flags (in message filenames) will be
# synchronized with the corresponding notmuch tags:
#
# Flag  Tag
# ----  -------
# D     draft
# F     flagged
# P     passed
# R     replied
# S     unread (added when 'S' flag is not present)
#
# The "notmuch new" command will notice flag changes in filenames and update
# tags, while the "notmuch tag" and "notmuch restore" commands will notice tag
# changes and update flags in filenames
synchronize_flags=true


[crypto]
#
# Name (or full path) of gpg binary to use in verification and
# decryption of PGP/MIME messages.
gpg_path=gpg2

